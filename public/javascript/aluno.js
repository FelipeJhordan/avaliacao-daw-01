/// <reference path="./lib/jquery-3.6.0.min.js" />

$(function(){
    $.post("/aluno", {})
        .done(function (json) {
            const pessoas = JSON.parse(json)
            setCards(pessoas)
        })
        .fail(function (jqXHR, textStatus, msg) {
            alert(msg);
        });


    $(".input-group").on("click", (event) => {
        sendSearch($("input").val())
    })
})

let sendSearch = (searchValue) => {
    $.post("/aluno", {search: searchValue} )
        .done( setCards)
        .fail(function (jqXHR, textStatus, msg) {
          alert(msg);
         });

}

let setCards = (pessoas = []) => {
    let html = ""
    pessoas.forEach( (pessoa) => {
        html += `
             <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
             <div class="card">
                <div class="card-body">
                    <h5 class="card-title">${pessoa.nome}</h5>
                    <p class="card-text">Email: ${pessoa.email}</p>
                </div>
            </div>
          </div>
        `
    })
    $("#section .row").html(html)
}