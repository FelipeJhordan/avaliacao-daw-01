/// <reference path="./lib/jquery-3.6.0.min.js" />

$(function(){
    $(".btn-submit").on("click", (event) => {
        let fields =  returnFields()
        if(fields.length != 2 ||  !$("#form-duvida")[0].checkValidity()) {
            $("#form-duvida")[0].checkValidity();
        } else {
            $("#form-duvida").addClass("form-hide")
            $("#title-section").addClass("animation-title-hidden")
            sendDuvida(fields)
        }
    })

    $("#form-duvida").on("animationend", (event) => {
        if(event.originalEvent.animationName === "down"){
          (document.querySelector("#form-duvida")).style.display = "none"
        }
    })

    $("#form-duvida").on("animationstart", (event) => {
        if(event.originalEvent.animationName === "down"){
          (document.querySelector("body")).style.overflow = "hidden"
        }
    })
})

let returnFields = ( () => {
    let x = []
    $('#form-duvida input').each((index, elemento)=>{
        if($(elemento).val().trim() != "")
        {
            x.length = x.length + 1
            x[$(elemento).attr('id')] = $(elemento).val();
        }
    })
    return x
})

let sendDuvida = (fields) => {
    $.post("/confirmacao", {...fields})
        .done( function (msg) {
           setTimeout(() => {
             $(".msg-email").addClass("msg-email-up")
             $(".email-sucess").text(msg)
             $("#title-section").text("Sucesso!") 
             $("#title-section").removeClass("animation-title-hidden")
             $("#title-section").addClass("animation-title-show")
           }, 400);
        })
        .fail(function (jqXHR, textStatus, msg) {
          alert(msg);
         });

}