/// <reference path="./lib/jquery-3.6.0.min.js" />

let getDataFormatted = () => { 
    const semana = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
    let actualDate = new Date()
    return `${semana[actualDate.getDay()]}, ${actualDate.getDate() + 1}/${actualDate.getMonth()}/${actualDate.getFullYear()}`
}


$(function () {
   $("#footer-date").text(getDataFormatted())
})