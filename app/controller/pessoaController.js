let fs = require("fs")
let { PATH_MOCK} = require("../utils/constants")

let fsPromise = fs.promises

class PessoaController {

    async returnMessage(req, res) {
        try {

            res.send("A dúvida com descrição " + req.body.descricao + " foi enviada com sucesso e será respondida no e-mail " + 
                req.body.email
                )
        } catch (err) {
            res.status(400).send(err.message)
        }
    }


     async getPessoa(req, res) {
        try {
            let data = await this.jsonRead()
            if(Object.keys(req.body).length === 0) {
                return res.send(data)
            } else {
                data = this.searchPessoaInArray(req.body.search, data)
            }
            res.send(data)
        } catch (err) {
            console.log(err)
            res.status(400).send(err.message)
        }
    }

    async jsonRead() {
        return await fsPromise.readFile( PATH_MOCK, "utf8")
    }

    searchPessoaInArray(searchWord, pessoas) {
        let pessoasArray = JSON.parse(pessoas)
        return pessoasArray.filter( ( {nome} ) => {
          if(String(nome).toUpperCase().indexOf(searchWord.toUpperCase()) != -1){
            return true
          }
          return false
        })
    }
}

module.exports = new PessoaController()