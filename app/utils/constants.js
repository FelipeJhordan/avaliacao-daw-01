let projectDir = __dirname.replace("app\\utils","")
module.exports = {
    DIR_PUBLIC: projectDir + "\\public\\",
    DIR_PAGES: projectDir  + "\\public\\html\\",
    PATH_ROUTES: projectDir + "\\app\\routes\\routes.js",
    PATH_PAGE_INDEX: projectDir + "\\public\\html\\index.html",
    PATH_PAGE_EU: projectDir + "\\public\\html\\eu.html",
    PATH_PAGE_ALUNO: projectDir + "\\public\\html\\aluno.html",
    PATH_PAGE_DUVIDA: projectDir + "\\public\\html\\duvida.html",
    PATH_MOCK: projectDir + "\\app\\mock\\alunos.json",
    PORT_SERVER: 3000
}
