var router = require("express").Router()
const pessoaController = require("../controller/pessoaController")
const constant = require ("../utils/constants.js")

router.get( "/" , (req, res) => {
    res.sendFile(constant.PATH_PAGE_INDEX)
})

router.get( "/eu" , (req, res) => {
    res.sendFile(constant.PATH_PAGE_EU)
})

router.get( "/duvida" , (req, res) => {
    res.sendFile(constant.PATH_PAGE_DUVIDA)
})

router.post( "/confirmacao", pessoaController.returnMessage.bind(pessoaController))

router.get( "/aluno" , (req, res) => {
    res.sendFile(constant.PATH_PAGE_ALUNO)
})

router.post( "/aluno" , pessoaController.getPessoa.bind(pessoaController))

module.exports = router